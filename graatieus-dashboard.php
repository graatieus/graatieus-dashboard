<?php
    /*
Plugin Name: Graatieus Dashboard
Version: 1.0
Author: Joris Graat
Author URI: https://graatieus.com
*/

    defined('ABSPATH') or die;

    class JG_Graatieus_Dashboard
    {
        public function __construct()
        {
            // Only add hooks if in admin
            if (is_admin()) {
                add_action('admin_menu', [$this, 'register_menu']);
                add_action('load-index.php', [$this, 'redirect_dashboard']);
                add_filter('admin_footer_text', [$this, 'modify_footer']);
                add_action('wp_before_admin_bar_render', [$this, 'remove_wp_logo']);
            }

            // Login page customizations
            add_action('login_enqueue_scripts', [$this, 'customize_login']);
            add_action('login_head', [$this, 'enqueue_login_styles']);
            add_filter('login_headerurl', [$this, 'login_logo_url']);
            add_filter('login_headertext', [$this, 'login_logo_title']);
        }

        public function register_menu()
        {
            add_dashboard_page(
                'Graatieus Dashboard',
                'Graatieus Dashboard',
                'read',
                'graatieus-dashboard-page',
                [$this, 'create_dashboard']
            );
        }

        public function redirect_dashboard()
        {
            $screen = get_current_screen();
            if (isset($screen->base) && 'dashboard' === $screen->base) {
                wp_redirect(admin_url('?page=graatieus-dashboard-page'));
                exit;
            }
        }

        public function create_dashboard()
        {
            include_once 'graatieus-dashboard-page.php';
        }

        public function modify_footer()
        {
            return '<span id="footer-thankyou">Deze website is ontwikkeld door <a href="http://www.graatieus.com" target="_blank">Graatieus</a></span>';
        }

        public function customize_login()
        {
        ?>
        <style type="text/css">
            body.login div#login h1 a {
                background-size: auto;
                background-image: url('<?php echo plugin_dir_url(__FILE__); ?>images/logo-login.svg');
            }
        </style>
        <?php
            }

                public function enqueue_login_styles()
                {
                    wp_enqueue_style('graatieus-custom', plugin_dir_url(__FILE__) . 'css/graatieus-custom.css');
                }

                public function login_logo_url()
                {
                    return 'https://graatieus.com/';
                }

                public function login_logo_title()
                {
                    return esc_html(get_bloginfo('name'));
                }

                public function remove_wp_logo()
                {
                    global $wp_admin_bar;
                    $wp_admin_bar->remove_menu('wp-logo');
                }
            }

        new JG_Graatieus_Dashboard();