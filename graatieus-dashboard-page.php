<?php
/**
 * Graatieus dashboard
 */

/** WordPress Administration Bootstrap */
require_once ABSPATH . 'wp-load.php';
require_once ABSPATH . 'wp-admin/admin.php';
require_once ABSPATH . 'wp-admin/admin-header.php';
?>
<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_URL(__FILE__); ?>styles/style.css" />

<div class="wrap about-wrap">
    <h1><?php _e('Welkom in het CMS!');?></h1>
    <h3>Heb je een vraag of verzoek?</h3>
    <p>Laat het mij gerust weten!</p>

    <div class="contact__block">
        <div class="contact__block__left">
            <img src="<?php echo plugin_dir_url(__FILE__) . 'images/joris.png'; ?>">
        </div>
        <div class="contact__block__right">
            <b>Joris Graat</b><br>
            <span>Graatieus | webdesign & ontwikkeling</span>
            <a href="mailto:joris@graatieus.nl" target="_blank" title="joris@graatieus.nl">joris@graatieus.nl</a><br>
            <a href="tel:06 - 17672642" target="_blank" class="phone" title="06 - 176726425">06 - 17672642</a>
        </div>
    </div>
</div>


<?php include ABSPATH . 'wp-admin/admin-footer.php';